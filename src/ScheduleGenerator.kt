
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import java.io.BufferedWriter
import java.io.File

fun main(args: Array<String>) {
    val numberOfLocations = args[0]
    println("Creating $numberOfLocations location schedule")
    generate_schedule(countryCodes = arrayOf("US", "GB"), numberOfLocations.toInt())
}


fun generate_schedule(countryCodes: Array<String>, numberOfLocations: Int) = runBlocking {
    val writer = File("lots_of_locations.csv").bufferedWriter()
    writer.write("lat,lon,tiv")
    writer.newLine()
    for (i in 1..numberOfLocations)
    launch {
        createLocation(countryCodes, writer)
    }

}


fun createLocation(countryCodes: Array<String>, writer: BufferedWriter) {
    val countryCode = countryCodes.toList().random()
    val boundingBoxes = BoundingBoxes.valueOf(countryCode).coordinates

    val lat = Math.random() * (boundingBoxes.get(3) - boundingBoxes.get(1)) + boundingBoxes.get(1);
    val lon = Math.random() * (boundingBoxes.get(2) - boundingBoxes.get(0)) + boundingBoxes.get(0);

    val tiv = Math.random() * (10000000000 - 1000) + 1000;
    println("$countryCode,$lat,$lon,$tiv")
    writer.write("$lat,$lon,$tiv")
    writer.newLine()
}

